document.querySelector('.reload').classList.add('hide');
let random = Math.floor(Math.random() * 20 ) + 1;
let brojac = 10;
-document.getElementById('proveri').addEventListener('click', function(){
    let unetiBroj = document.getElementById('unos').value;
    if(isNaN(unetiBroj) || unetiBroj <1 || unetiBroj >20){
        document.getElementById('rezultat').innerHTML = 'Nešto nije kako treba.<br>Prihvaćeni su samo brojevi od 1 do 20.';
    }
    else if(unetiBroj > random){
        document.getElementById('rezultat').textContent = `Broj ${unetiBroj} je veći od tačnog.`;
        --brojac;
        document.getElementById('brojac').textContent = brojac;
    }
    else if(unetiBroj < random){
        document.getElementById('rezultat').textContent = `Broj ${unetiBroj} je manji od tačnog.`;
        --brojac;
        document.getElementById('brojac').textContent = brojac;

    }
    else if(unetiBroj == random){
        document.getElementById('rezultat').textContent = `Bravo.👏 ${unetiBroj} je tačan broj.`;
        document.getElementById('proveri').value = '🔄';
        document.querySelector('.reload').classList.remove('hide');
    }

    if(brojac <= 0){
        document.getElementById('rezultat').textContent = '💥 Izgubio si... 💥';
        document.getElementById('brojac').textContent = '0';
        document.getElementById('proveri').value = '🔄';
        document.querySelector('.reload').classList.remove('hide');
    }
})
document.querySelector('.reload').addEventListener('click', function(){
    location.reload();
})